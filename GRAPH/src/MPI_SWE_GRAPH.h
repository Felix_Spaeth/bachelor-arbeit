/* MPI Header for the MPI exchange with SWE_FRAVE
*
*	Copyright(C) 2015  Felix Spaeth
*
*	This Document is under the CC by-nc 3.0 license
*/

#ifndef MPI_SWE_GRAPH_H_
#define MPI_SWE_GRAPH_H_

#include "mpi.h"

#include <stdio.h>
#include <stdlib.h>
#include <cmath>
#include <string>
#include <stddef.h>
#include <mgl2/fltk.h>



struct PointForMPI{
	int x, y;
	float h, b, time;
};

struct DataLinkedList{
	int x, y;
	int elements, current;  //size of mglData, #currently entered
	mglData h,b,t;
	DataLinkedList *next;
};

class MPI_SWE_GRAPH {
public:
	MPI_SWE_GRAPH();
	virtual ~MPI_SWE_GRAPH();

	void init(int * argc, char *argv[]);
	void finalize();
	void abort();

	void exchangeStatus(bool &doReset, bool &stop, bool &newScenario, int &scenario,
			DataLinkedList *pointsList, int x=-1, int y=-1, int numberOfElements=0);
	float maxTimeStep(float l_maxTimeStepWidth=1000);
	void receivePoints(DataLinkedList *PointsToUpdate);
	void registerPoint(DataLinkedList *pointsList, int x, int y, int numberOfElements);

	int getHeight(){
		//printf("height: %d\n", height);
		return height;};
	int getWidth(){
		//printf("width: %d\n", width);
		return width;};
	int getBlocksX(){
		//printf("blocksX: %d\n", blocksX);
		return blocksX;
	};
	int getBlocksY(){
		//printf("blocksY: %d\n", blocksY);
		return blocksY;};

private:
	int computeNumberOfBlockRows();
	void updatePoint(DataLinkedList *PointsToUpdate, PointForMPI point);


	//! MPI Rank of a process.
	int mpiRank;
	//! number of MPI processes.
	int numberOfProcesses;

	MPI_Datatype mpiPoint;

	int blocksX, blocksY;
	int width, height;

};

#endif /* MPI_SWE_GRAPH_H_ */
