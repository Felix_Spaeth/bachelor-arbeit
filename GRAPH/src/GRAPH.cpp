/* Main Class for displaying the Graph
*
*	Copyright(C) 2015  Felix Spaeth
*
*	This Document is under the CC by-nc 3.0 license
*/




#include "mpi.h"
#include <mgl2/fltk.h>
#include <FL/Fl_Double_Window.H>
#include <Fl/Fl_Scroll.H>
#include <Fl/Fl_Input.H>
#include <Fl/Fl_Output.H>
#include <FL/Fl_Choice.H>


#include "MPI_SWE_GRAPH.h"


#define NUMBER_OF_ELEMENTS 150
mglGraph *gr=NULL;
Fl_Input *xInput=NULL;
Fl_Input *yInput=NULL;
Fl_Button *okButton=NULL;
Fl_Button *pauseButton=NULL;
Fl_Button *resetButton=NULL;
Fl_Output *outPut=NULL;
Fl_Scroll *scr=NULL;
Fl_MathGL *mgl=NULL;
Fl_Choice *choice=NULL;
Fl_Menu_Item *choice_Items=NULL;
//mglData *data=NULL;
//int countData;
DataLinkedList *pointsList=NULL;


MPI_SWE_GRAPH *mpi=NULL;
bool doReset=false;
bool pause=false;
bool newScenario=false;
int scenario=0;
int xToRegister=-1, yToRegister=-1;
std::ostringstream os;
std::string error;


template<typename T>
std::string toString(T x)
{
    std::ostringstream result;
    result << x;
    return result.str();
}

/**
 * callback for PauseButton
 * @param buttonptr unused
 */
void buttonPause_cb(Fl_Widget* buttonptr){

	pause= !pause;
	printf("pause: %d", pause);
}

/**
 * callback for OKButton
 * @param buttonptr unused
 */
void buttonOK_cb(Fl_Widget* buttonptr){
	int x=-1, y=-1;
	x= atoi(xInput->value());
	y= atoi(yInput->value());

	printf("Point to register: x: %d, y: %d\n", y, x);
	if(x >= 0 && x <= (mpi->getBlocksX() * mpi->getWidth())
	    && y >= 0 && y <= (mpi->getBlocksY() * mpi->getHeight())){
		xToRegister=x;
		yToRegister=y;

		if(pointsList==NULL){
			DataLinkedList *next= new DataLinkedList;
				next->x=x; next->y=y;
				next->elements=NUMBER_OF_ELEMENTS; next->current=0;
				next->h = new mglData(NUMBER_OF_ELEMENTS);
				next->b = new mglData(NUMBER_OF_ELEMENTS);
				next->t = new mglData(NUMBER_OF_ELEMENTS);
				next->next = NULL;
				for(int i=0;i<=NUMBER_OF_ELEMENTS;i++){
					next->h[i]=NAN;
					next->b[i]=NAN;
					next->t[i]=NAN;
				}
			pointsList=next;
		}else{
			DataLinkedList *tmp=pointsList;
			while(tmp->next!=NULL){
				tmp=tmp->next;
			}
			DataLinkedList *next= new DataLinkedList;
				next->x=x; next->y=y;
				next->elements=NUMBER_OF_ELEMENTS; next->current=0;
				next->h = new mglData(NUMBER_OF_ELEMENTS);
				next->b = new mglData(NUMBER_OF_ELEMENTS);
				next->t = new mglData(NUMBER_OF_ELEMENTS);
				next->next = NULL;
				for(int i=0;i<=NUMBER_OF_ELEMENTS;i++){
					next->h[i]=NAN;
					next->b[i]=NAN;
					next->t[i]=NAN;
				}
			tmp->next=next;
		}
	}else{

		//os.str().c_str()
		outPut->label(error.c_str());
		printf("%d <= %d, %d <= %d\n", x, (mpi->getBlocksX() * mpi->getWidth()), y, (mpi->getBlocksY() * mpi->getHeight()));
	}
}

/**
 * returns number of messurement points
 * @param points
 * @return #messurement points
 */
int getNumDatasets(DataLinkedList *points){
	if(points==NULL)return 0;
	else return 1 + getNumDatasets(points->next);
}

/**
 * called on reset, deletes all received Data
 */
void reset(){
	printf("reset()");

	DataLinkedList *next=pointsList;
	while(next!=NULL){

		for(int i=0;i<=NUMBER_OF_ELEMENTS;i++){
			next->h[i]=NAN;
			next->b[i]=NAN;
			next->t[i]=NAN;
		}
		next->current = 0;
		next=next->next;
	}
}

/**
 * Callback method that closes the Program
 * @param
 * @param
 */
void cb_close(Fl_Widget*, void*){
	exit(0);
}

/**
 * callback to switche scenario
 * @param arg new scenraio
 */
void cb_scenario(Fl_Widget*, void* arg){
	int sc=*((int*)(&arg));
	scenario=sc;
	newScenario=true;
}

/**
 * callback to reset the current scenario
 * @param
 * @param
 */
void cb_reset(Fl_Widget*, void*){
	doReset=true;
}

int main(int argc, char **argv)
{
	//MPI:
	mpi= new MPI_SWE_GRAPH();
	mpi->init(&argc, argv);


	//Window
    Fl_Double_Window Wnd(800,600,"Graph");
    Wnd.color(FL_YELLOW);
    Wnd.begin();
    	int x=20;
		int y = 10;
    	xInput = new Fl_Input(x, y, 70, 30, "X:"); x+=100;
    	yInput = new Fl_Input(x, y, 70, 30, "Y:"); x+=90;
    	okButton = new Fl_Button(x, y, 50, 30, "OK");
    	okButton->callback(buttonOK_cb);

    	std::ostringstream os1;
    	os1 << "max X: " << mpi->getBlocksX() * mpi->getWidth() << " max Y: " << mpi->getBlocksY() * mpi->getHeight();

    	std::string str=os1.str();
    	x+=(str.size()+9)*7 +70;
    	outPut = new Fl_Output(x, y, 0, 30, str.c_str());    	x+=10;
    	resetButton = new Fl_Button(x, y, 70, 30, "Reset"); 	x+=80;
    	resetButton->callback(cb_reset);
    	pauseButton = new Fl_Button(x, y, 70, 30, "Pause");    x+=150;
    	pauseButton->callback(buttonPause_cb);

    	choice =new Fl_Choice(x, y, 100, 30, "Scenario: "); x+=130;

    	choice_Items=new Fl_Menu_Item[7];
    	for(int i=0;i<6;i++)
    		choice_Items[i].text =NULL;
    	choice_Items->add("radial", 1, cb_scenario, (void*) 0, 0);
    	choice_Items->add("leftBottom", 2, cb_scenario, (void*) 1, 0);
    	choice_Items->add("radial2", 3, cb_scenario, (void*) 2, 0);
    	choice_Items->add("left", 4, cb_scenario, (void*) 3, 0);
    	choice_Items->add("chile", 5, cb_scenario, (void*) 4, 0);
    	choice_Items->add("tohoku", 6, cb_scenario, (void*) 5, 0);
    	choice->menu(choice_Items);


    	scr = new Fl_Scroll(0, 50, 800, 550);
    	mgl = new Fl_MathGL(0, 0, 0, 0);
    Wnd.end();
    Wnd.callback(cb_close);
    Wnd.resizable(scr);
    Wnd.show();
    //End Window

    gr = new mglGraph(mgl->get_graph());

    gr->Box();    gr->Axis();

	os << "nicht in max X: " << mpi->getBlocksY() * mpi->getWidth() << " max Y: " << mpi->getBlocksX() * mpi->getHeight();
	error=os.str();

    while(true){
    	bool f=false;
    	mpi->exchangeStatus(doReset, f, newScenario, scenario, pointsList,
    			xToRegister, yToRegister, NUMBER_OF_ELEMENTS);
    	xToRegister=-1;
    	yToRegister=-1;

    	//if(pause)pause=false;
    	newScenario=false;

		if (pause){
			mpi->maxTimeStep(0); //do not safe received points
			mpi->receivePoints(NULL);
		}else{
			mpi->maxTimeStep(1000);
			mpi->receivePoints(pointsList);
		}

    	if(doReset){
			reset();
		}

    	newScenario=false;

    	gr->Clf();


    	DataLinkedList *next=pointsList;
        int current=0;
    	while(next!=NULL){
    		gr->SubPlot(1, getNumDatasets(pointsList), current);

	    	std::string str="X: "+toString(next->x)+" Y: "+toString(next->y);
	    	gr->Title(str.c_str());
	    	if(doReset||next->current<2){
	    		gr->SetRange('x', pointsList->t);
				gr->SetRange('y', -0.1, 0.1);
	    	}else{
				gr->SetRange('x', pointsList->t);
				gr->SetRange('y', next->h, true);
	    	}
			gr->SetTicksTime('x',0, "%H:%M:%S");
			gr->Axis();

    		gr->Plot(next->t, next->h, "b");
			gr->Box();

    		next=next->next;
            current++;
    	}
		doReset=false;
		mgl->update();

		if(!pause)
			Fl::wait(0.00001);
		else
			Fl::wait(0.1);
    }

    return Fl::run();


}



