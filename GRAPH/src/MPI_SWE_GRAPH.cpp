/* Main Class for the MPI exchange with SWE_FRAVE
*
*	Copyright(C) 2015  Felix Spaeth
*
*	This Document is under the CC by-nc 3.0 license
*/

#include "MPI_SWE_GRAPH.h"

#define RUNNING 100
#define RESET 101
#define STOP 102
#define REGISTER_POINT 103
#define POINT_TAG_OLD 2021

MPI_SWE_GRAPH::MPI_SWE_GRAPH()
{
	// TODO Auto-generated constructor stub

}

MPI_SWE_GRAPH::~MPI_SWE_GRAPH() {
	// TODO Auto-generated destructor stub
	MPI_Type_free(&mpiPoint);
}


/**
 * initialises MPI
 * if there is a Graph it recognizes it
 * @param argc #Paramters for MPI
 * @param argv Parameters for MPI
 */
void MPI_SWE_GRAPH::init(int *argc, char* argv[]){
	// initialize MPI
	if (MPI_Init(argc, &argv) != MPI_SUCCESS) {
		printf("MPI_Init failed.");
	}

	// determine local MPI rank
	MPI_Comm_rank(MPI_COMM_WORLD, &mpiRank);
	// determine total number of processes
	MPI_Comm_size(MPI_COMM_WORLD, &numberOfProcesses);

	int ownStatus[] = { mpiRank };
	int recv[numberOfProcesses];
	MPI_Allgather(&ownStatus, 1, MPI_INT, &recv, 1, MPI_INT, MPI_COMM_WORLD);

	//MPIDatatype mpiPoint
	const int nitems=5;
	int blocklengths[5]={1,1,1,1, 1};
	MPI_Datatype types[5] = {MPI_INT, MPI_INT, MPI_FLOAT, MPI_FLOAT, MPI_FLOAT};
	MPI_Aint offsets[5];
	offsets[0] = (int) offsetof(struct PointForMPI, x);
	offsets[1] = (int) offsetof(struct PointForMPI, y);
	offsets[2] = (int) offsetof(struct PointForMPI, h);
	offsets[3] = (int) offsetof(struct PointForMPI, b);
	offsets[4] = (int) offsetof(struct PointForMPI, time);

	MPI_Type_create_struct(nitems, blocklengths, offsets, types, &mpiPoint);
	MPI_Type_commit(&mpiPoint);

	// determine the layout of MPI-ranks: use blocksX*blocksY grid blocks
	//printf("computeRows\n");
	blocksY = computeNumberOfBlockRows();
	//printf("gotX: %d\n", blocksX);
	blocksX = (numberOfProcesses-1) / (blocksY>0?blocksY : 1);
	//printf("gotY: %d\n", blocksY);


	//setSize
	int ownHW=0;
	int globalW;
	//printf("before Reduce");
	MPI_Allreduce(&ownHW, &globalW, 1, MPI_INT,
			MPI_MAX, MPI_COMM_WORLD);

	printf("after fst reduce");
	int globalH;
	MPI_Allreduce(&ownHW, &globalH, 1, MPI_INT,
			MPI_MAX, MPI_COMM_WORLD);
	//printf("after both reduce");

	width=globalW;
	height=globalH;
	//printf("MPI_SWE_GRAPH: width: %d, height: %d\n", width, height);

}

/**
 * closes MPI connection
 */
void MPI_SWE_GRAPH::finalize(){
	MPI_Finalize();
}

/**
 * aborts MPI connection
 */
void MPI_SWE_GRAPH::abort(){
	MPI_Abort(MPI_COMM_WORLD, -1);
}

/**
 * exchanges the status
 * @param doReset
 * @param stop
 * @param newScenario
 * @param scenario
 * @param pointsList
 * @param regX >0 x of Point to add
 * @param regY >0 y of Point to add
 * @param numberOfElements unused
 */
void MPI_SWE_GRAPH::exchangeStatus(bool &doReset, bool &stop, bool &newScenario, int &scenario,
		DataLinkedList *pointsList, int regX, int regY, int numberOfElements){
	/*
		 * 100 running
		 * 101 reset
		 * 102 stop
		 *
		 * 0-NUM_OF_SCENARIOS sceanrios
		 */
		int ownStatus[] = { RUNNING };
		if (doReset)
			ownStatus[0] = RESET;
		if (stop)
			ownStatus[0] = STOP;
		if (newScenario)
			ownStatus[0] = scenario;
		if(regX>=0&&regY>=0)
			ownStatus[0] = REGISTER_POINT;

		int recv[numberOfProcesses];
		MPI_Allgather(&ownStatus, 1, MPI_INT, &recv, 1, MPI_INT, MPI_COMM_WORLD);

		for (int i = 0; i < numberOfProcesses; i++) {
			switch (recv[i]) {
			case RESET:
				doReset = true;
				break;
			case STOP:
				stop = true;
				break;
			case REGISTER_POINT:
				registerPoint(pointsList, regX, regY, numberOfElements);
				break;
			case RUNNING:
				break;
			default:
				if (recv[i] >= 0 && recv[i] < 100) {
					doReset = true;
					newScenario = false;
				}
			}
		}
}


/**
 * exchanges the maximum Timestep
 * @param l_maxTimeStepWidth use 0 to stop animation otherwise a big number
 * @return
 */
float MPI_SWE_GRAPH::maxTimeStep(float l_maxTimeStepWidth){
	float l_maxTimeStepWidthGlobal = 0;
	MPI_Allreduce(&l_maxTimeStepWidth, &l_maxTimeStepWidthGlobal, 1, MPI_FLOAT,
			MPI_MIN, MPI_COMM_WORLD);
	return l_maxTimeStepWidthGlobal;
}

/**
 * receives pointdata as long as there is any in the queue
 * @param PointsToUpdate
 */
void MPI_SWE_GRAPH::receivePoints(DataLinkedList *PointsToUpdate){
	int flag=0;
	MPI_Status *status= new MPI_Status();
	MPI_Iprobe(MPI_ANY_SOURCE, POINT_TAG_OLD, MPI_COMM_WORLD, &flag, status);
	while(flag){
		PointForMPI rec[1];
		MPI_Recv(rec, 1, mpiPoint, MPI_ANY_SOURCE, POINT_TAG_OLD, MPI_COMM_WORLD, status);
		updatePoint(PointsToUpdate, rec[0]);

		MPI_Iprobe(MPI_ANY_SOURCE, POINT_TAG_OLD, MPI_COMM_WORLD, &flag, status);
	}

}

/**
 * Adds the new information to the Point
 * @param PointsToUpdate
 * @param point
 */
void MPI_SWE_GRAPH::updatePoint(DataLinkedList *PointsToUpdate, PointForMPI point){
	if(PointsToUpdate!=NULL){
		//printf("rec: x: %d, y: %d, h: %f, b: %f, t: %f\n", s.x, s.y, s.h, s.b, s.time);
		if(PointsToUpdate->x == point.x && PointsToUpdate->y == point.y){
			if(PointsToUpdate->current < PointsToUpdate->elements){
				PointsToUpdate->h[PointsToUpdate->current] = point.h+point.b;
				PointsToUpdate->b[PointsToUpdate->current] = point.b;
				PointsToUpdate->t[PointsToUpdate->current] = point.time-3600.0;
				PointsToUpdate->current+=1;
			}else{
				for(int i=0;i<PointsToUpdate->elements; i++){
					PointsToUpdate->h[i] =PointsToUpdate->h[i+1];
					PointsToUpdate->b[i] =PointsToUpdate->b[i+1];
					PointsToUpdate->t[i] =PointsToUpdate->t[i+1];
				}
				PointsToUpdate->h[PointsToUpdate->elements -1]= point.h+point.b;
				PointsToUpdate->b[PointsToUpdate->elements -1]= point.b;
				PointsToUpdate->t[PointsToUpdate->elements -1]= point.time-3600.0;
			}
		}else{
			if(PointsToUpdate->next!=NULL)
				updatePoint(PointsToUpdate->next, point);
		}
	}
}

/**
 * registers a new messurement Point
 * @param pointsList linked list of points, unused
 * @param x xPosition of new Point in global koordspace
 * @param y yPosition of new Point in global koordspace
 * @param numberOfElements unused
 */
void MPI_SWE_GRAPH::registerPoint(DataLinkedList *pointsList, int x, int y, int numberOfElements){

		int send[]={x, y};
		MPI_Bcast(send, 2, MPI_INT, mpiRank ,MPI_COMM_WORLD);

}

/**
 * computes the number of rows
 * @return suggested number of rows
 */
int MPI_SWE_GRAPH::computeNumberOfBlockRows(){
	int numberOfCols = std::sqrt(numberOfProcesses-1);
	if(numberOfCols==0)return 1;
	while ((numberOfProcesses-1) % numberOfCols != 0)
		numberOfCols--;
	return numberOfCols;
}

