﻿/**
 * @file
 * This file is part of SWE.
 *
 * @author Jakob Rott
 *
 * @section LICENSE
 *
 * SWE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * SWE is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with SWE.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * @section DESCRIPTION
 *
 * TODO
 */

#ifndef SWETSUSCENARIO_HPP_
#define SWETSUSCENARIO_HPP_

//Error handling NetCDF: printing err-msg, exiting non-zero
#define ERR(e) {printf("Error: %s\n", nc_strerror(e)); assert(false);}

#include <cassert>
#include <string>
#include <netcdf.h>
#include "SWE_Scenario.hh"


class SWE_TsunamiScenario: public SWE_Scenario {
//private:
	
	// Error handling NetCDF
	int retval, status; 
	
	// NetCDF ID
	int ncidBath, ncidDisp;
	
	int varid, dispZId;
	size_t varlength;
	float ip;
	
	// distance between two entries
	float dxBath, dyDisp, dyBath, dxDisp;
	
    // secs until end of the simulation
    float duration;

    // ranges of the bathymetry file, 0: min(x), 1: max(x), 2: min(y), 3: max(y)
    float bathymetryRange[4];

    // ranges of the displacement file, 0: min(x), 1: max(x), 2: min(y), 3: max(y)
    float displacementRange[4];
	
	BoundaryType bL, bR, bT, bB;
	
	std::string bathFile;
	std::string dispFile;
	

  public:
    /**
     * Constructor of a Tsunami Scenario
	 * Reads .nc Files and calculates SWE Scenario
     *
     */
   SWE_TsunamiScenario ( std::string i_BathFile, std::string i_DispFile,
		   BoundaryType i_bL=OUTFLOW, BoundaryType i_bR=OUTFLOW,
		   BoundaryType i_bT=OUTFLOW, BoundaryType i_bB=OUTFLOW, float i_dur=0.0f)
   {
   		duration = i_dur;

	    bL = i_bL;
	    bR = i_bR;
	    bT = i_bT;
	    bB = i_bB;
	   
  		if ((retval = nc_open(i_BathFile.c_str(), NC_NOWRITE, &ncidBath)))
  			ERR(retval);

		// Bathymetry Range X - begin
		status = nc_inq_dimid(ncidBath, "x", &varid);  
		// std::cout << "   Bathymetry Range X - dimid - " << varid << std::endl;
		if (status != NC_NOERR)
			ERR(retval);
		
		status = nc_inq_dimlen(ncidBath, varid, &varlength); 
		// std::cout << "   Bathymetry Range X - dimlen" << varlength << std::endl;
		if (status != NC_NOERR) 
			ERR(retval);
		
	    if ((retval = nc_inq_varid(ncidBath, "x", &varid)))
	        ERR(retval);
		// std::cout << "   Bathymetry Range X - varid" << varid << std::endl;


		size_t start[1];
		start[0] = 0;
		
		nc_get_var1_float (ncidBath, varid,	start, &ip);
		bathymetryRange[0] = ip;
		// std::cout << "   Bathymetry Range X - 1st float" << ip << std::endl;

		start[0] = varlength-1;
		
		nc_get_var1_float (ncidBath, varid,	start, &ip);
		bathymetryRange[1] = ip;
		
		std::cout << "   Bathymetry Range X - last float" << bathymetryRange[1] << std::endl;
		// Bathymetry Range X - end
		
		
		// Bathymetry Range Y - begin
		status = nc_inq_dimid(ncidBath, "y", &varid);  
		// std::cout << "   Bathymetry Range Y - dimid - " << varid << std::endl;
		if (status != NC_NOERR)
			ERR(retval);
		
		status = nc_inq_dimlen(ncidBath, varid, &varlength); 
		if (status != NC_NOERR) 
			ERR(retval);
		// std::cout << "   Bathymetry Range Y - dimlen" << varlength << std::endl;

		
	    if ((retval = nc_inq_varid(ncidBath, "y", &varid)))
	       ERR(retval);
		// std::cout << "   Bathymetry Range Y - varid" << varid << std::endl;


		start[0] = 0;
		
		nc_get_var1_float (ncidBath, varid,	start, &ip);
		bathymetryRange[2] = ip;
		// std::cout << "   Bathymetry Range Y - 1st float" << ip << std::endl;

		
		start[0] = varlength-1;
		
		nc_get_var1_float (ncidBath, varid,	start, &ip);
		bathymetryRange[3] = ip;		
		// std::cout << "   Bathymetry Range Y - last float" << ip << std::endl;

		// Bathymetry Range Y - end


		// Calc dxBath - begin
		start[0] = 0;
		if ((retval = nc_get_var1_float (ncidBath, varid, start, &dxBath)))
	        ERR(retval);

		start[0] = 1;
		if ((retval = nc_get_var1_float (ncidBath, varid, start, &ip)))
	        ERR(retval);
		
		dxBath = abs(dxBath - ip);
		// std::cout << "   dxBath" << dxBath << std::endl;
		// Calc dxBath - end

		// Calc dyBath - begin
		size_t starte[2];
		starte[0] = 0;
		starte[1] = 0;
			
		if ((retval = nc_get_var1_float (ncidBath, varid, starte, &dyBath)))
	        ERR(retval);

		starte[0] = 1;
		if ((retval = nc_get_var1_float (ncidBath, varid, starte, &ip)))
	        ERR(retval);
		
		dyBath = abs(dyBath - ip);
		// std::cout << "   dyBath" << dyBath << std::endl;
		// Calc dyBath - end
		
		// std::cout << "   varid (z)" << varid << std::endl;
		
  		if ((retval = nc_open(i_DispFile.c_str(), NC_NOWRITE, &ncidDisp)))
  			ERR(retval);
		
		
		// Displacement Range X - begin
		status = nc_inq_dimid(ncidDisp, "x", &varid);  
		if (status != NC_NOERR)
			ERR(retval);
		
		status = nc_inq_dimlen(ncidDisp, varid, &varlength); 
		if (status != NC_NOERR) 
			ERR(retval);
		
	    if ((retval = nc_inq_varid(ncidDisp, "x", &varid)))
	        ERR(retval);

		start[0] = 0;
		
		nc_get_var1_float (ncidDisp, varid,	start, &ip);
		displacementRange[0] = ip;
		// std::cout << "   Displacement Range X - first float" << ip << std::endl;

		
		start[0] = varlength-1;
		
		nc_get_var1_float (ncidDisp, varid,	start, &ip);
		displacementRange[1] = ip;
		// std::cout << "   Displacement Range X - last float" << ip << std::endl;
		// Displacement Range X - end
		
		
		// Bathymetry Range Y - begin
		status = nc_inq_dimid(ncidDisp, "y", &varid);  
		if (status != NC_NOERR)
			ERR(retval);
		
		status = nc_inq_dimlen(ncidDisp, varid, &varlength); 
		if (status != NC_NOERR) 
			ERR(retval);
		
	    if ((retval = nc_inq_varid(ncidDisp, "y", &varid)))
	       ERR(retval);

		start[0] = 0;
		
		nc_get_var1_float (ncidDisp, varid,	start, &ip);
		displacementRange[2] = ip;
		// std::cout << "   Displacement Range Y - first float" << ip << std::endl;
		
		start[0] = varlength-1;
		
		nc_get_var1_float (ncidDisp, varid,	start, &ip);
		displacementRange[3] = ip;		
		// std::cout << "   Displacement Range Y - last float" << ip << std::endl;
		// Bathymetry Range Y - end

		// Calc dxDisp - begin
		start[0] = 0;
		if ((retval = nc_get_var1_float (ncidDisp, varid, start, &dxDisp)))
	        ERR(retval);

		start[0] = 1;
		if ((retval = nc_get_var1_float (ncidDisp, varid, start, &ip)))
	        ERR(retval);
		
		dxDisp = abs(dxDisp - ip);
		std::cout << "   dxDisp" << dxDisp << std::endl;
		// Calc dxDisp - end

		// Calc dyDisp - begin
		starte[0] = 0;
		starte[1] = 0;
		if ((retval = nc_get_var1_float (ncidDisp, varid, starte, &dyDisp)))
	        ERR(retval);


		starte[0] = 1;
		if ((retval = nc_get_var1_float (ncidDisp, varid, starte, &ip)))
	        ERR(retval);

		dyDisp = abs(dyDisp - ip);
		// std::cout << "   dyDisp" << dyDisp << std::endl;
		// Calc dyDisp - end
		
		// preparing for coming search
		// dispZId -> z in Displacement File
	    if ((retval = nc_inq_varid(ncidDisp, "z", &dispZId)))
	        ERR(retval);
		
		// preparing for coming search
	    if ((retval = nc_inq_varid(ncidBath, "z", &varid))) 		// varid now holds id of z in bathymetry file
	        ERR(retval);
		
    }

    ~SWE_TsunamiScenario(){
		if ((retval = nc_close(ncidBath)))
  		   ERR(retval);
		
		if ((retval = nc_close(ncidDisp)))
  		   ERR(retval);
    }

   /**
    * Find my nearby(est) existing neighbour
    *
    * @param xy, is a x or y coorinate minus the first value (range)
	* @param dxy, is the distance between two entries
    * @return right value where data exists
    */
   float getNearby(float xy, float dxy)  {
	   int temp;
	   float ret;
	   temp = (int) (xy / dxy);

	   ret = (float)temp * dxy;
	   
	   if (abs(ret - xy) < abs(ret+dxy-xy))
		   return ret;
	   // std::cout << "   getNearby: Ret " << ret << std::endl;
	   ret += dxy;
	   // std::cout << "   getNearby: Ret " << ret << std::endl;
	   return ret;
   }

    /**
     * Get the water height at a specific location (before the initial displacement).
     *
     * @param i_positionX position relative to the origin of the bathymetry grid in x-direction
     * @param i_positionY position relative to the origin of the bathymetry grid in y-direction
     * @return water height (before the initial displacement)
     */
    float getWaterHeight(float x, float y)  {
    	//float t=y;
    	//y=x;
    	//y=max[y]-y+min[y]
    	//y=bathymetryRange[3]-y+bathymetryRange[2];  //flips the frame, thus North is North
    	//x=t;

		float ret;
		ret = getBathymetryBefore(x,y);
		// if (ret == -485)
		// 	std::cout << "   waterheight : " << ret << " x " << x << " y " << y << std::endl;
	    if (ret <= -20)
			return -ret;
		else if (ret < 0)
			return 20;

		return 0;
    }

    /**
     * Get the bathymetry + displacement
     *
     * @param i_positionX position relative to the origin of the displacement grid in x-direction
     * @param i_positionY position relative to the origin of the displacement grid in y-direction
     * @return bathymetry (incl. displacement)
     */
    float getBathymetry(float x, float y) {
    	//float t=y;
    	//y=x;
    	//y=max(y)-y+min(y)
    	//y=bathymetryRange[3]-y+bathymetryRange[2]; //flips the frame, thus North is North
    	//x=t;
        
        //netCDF can't handle values lower than the actual range
        //but getB... is called with bathymetryRange[0]-0.5*dx from SWE_BLOCK
        if(x<bathymetryRange[0])
            x=bathymetryRange[0];
        if(x>bathymetryRange[1])
            x=bathymetryRange[1]-1;
        if(y<bathymetryRange[2])
            y=bathymetryRange[2];
        if(y>bathymetryRange[3])
            y=bathymetryRange[3]-1;

		float bathy = getBathymetryBefore(x,y) + getDisplacement(x,y);
		
		if (bathy > -20 && bathy <= 0)
			bathy = -20;
		
		if (bathy < 20 && bathy > 0)
			bathy = 20;
		
		return bathy;
    }

   /**
    * Get the bathymetry before tsunami
    *
    * @param x position relative to the origin of the displacement grid in x-direction
    * @param y position relative to the origin of the displacement grid in y-direction
    * @return bathymetry before tsunami w/o displacement
    */
   float getBathymetryBefore(float x, float y) {
		float start[2];

		// std::cout << "   getBathymetryBefore: Input x " << x << " y " << y << std::endl;
		// std::cout << "   getBathymetryBefore: To Nearby x " << x-bathymetryRange[0] << " dxBath " << dxBath << std::endl;

		start[0] = getNearby(x-bathymetryRange[0], dxBath) / dxBath;
		// std::cout << "   getBathymetryBefore: To Nearby y " << y-bathymetryRange[2] << " dyBath " << dxBath << std::endl;

		start[1] = getNearby(y-bathymetryRange[2], dyBath) / dyBath;

		// size_t ind[] = {start[1] * dyBath + bathymetryRange[2], start[0] * dxBath + bathymetryRange[0]};

		size_t ind[] = {start[1], start[0]};
		// std::cout << "   getBathymetryBefore: Now X " << start[0] * dxBath + bathymetryRange[0] << " Y " << start[1] * dyBath + bathymetryRange[2] << "bathRange0"<<bathymetryRange[0] << std::endl;
 		if ((retval = nc_get_var1_float (ncidBath, varid, ind, &ip)))
 			ERR(retval);

		// std::cout << ip << " x " << ind[1] << " y " << ind[0] << std::endl;
		// std::cout << "   getBathymetryBefore: Res " << ip << std::endl;
		return ip;
   }


   /**
    * Get displacement
    *
    * @param x position relative to the origin of the displacement grid in x-direction
    * @param y position relative to the origin of the displacement grid in y-direction
    * @return displacement in (x|y)
    */
   float getDisplacement( float x, float y ) {
	   
	   if ((x < displacementRange[0]) | (x > displacementRange[1]) | (y < displacementRange[2]) | (y > displacementRange[3] ))
		   return 0;
	   
	    float start[2];
		start[0] = getNearby(x-displacementRange[0], dxDisp) / dxDisp;
		start[1] = getNearby(y-displacementRange[2], dyDisp) / dyDisp;

		// size_t ind[] = {start[1] * dyDisp + displacementRange[2], start[0] * dxDisp + displacementRange[0]};

		size_t ind[] = {start[1], start[0]};

 		if ((retval = nc_get_var1_float (ncidDisp, dispZId, ind, &ip)))
 			ERR(retval);

		// if (ip > 1000)
		// std::cout << "   getDisplacement " << ip << std::endl;

		return ip;
   }

    /**
     * Get the number of seconds, the simulation should run.
     * @return number of seconds, the simulation should run
     */
    float endSimulation() {
      return duration;
    };

    /**
     * Get the boundary types of the simulation
     * @param edge specific edge
     * @return type of the edge
     */
    BoundaryType getBoundaryType( BoundaryEdge i_edge ) {
        if ( i_edge == BND_LEFT )
          return bL;
        else if ( i_edge == BND_RIGHT)
          return bR;
        else if ( i_edge == BND_BOTTOM )
          return bB;
        else
          return bT;
    }

    /** Get the boundary positions
     *
     * @param i_edge which edge
     * @return value in the corresponding dimension
     */
    float getBoundaryPos(BoundaryEdge i_edge) {
       if ( i_edge == BND_LEFT )
         return bathymetryRange[0];
       else if ( i_edge == BND_RIGHT)
         return bathymetryRange[1];
       else if ( i_edge == BND_BOTTOM )
         return bathymetryRange[2];
       else
         return bathymetryRange[3];

//    	//with 90° rotation
//        if ( i_edge == BND_LEFT )
//          return bathymetryRange[2];
//        else if ( i_edge == BND_RIGHT)
//          return bathymetryRange[3];
//        else if ( i_edge == BND_BOTTOM )
//          return bathymetryRange[0];
//        else
//          return bathymetryRange[1];
    };
};

#endif /* SWETSUSCENARIO_HPP_ */
