/* Copies the data of float2D arrays to the GPU
*
*	Copyright(C) 2015  Felix Spaeth
*
*	This program is free software : you can redistribute it and / or modify
*	it under the terms of the GNU General Public License as published by
*	the Free Software Foundation, either version 3 of the License, or
*	(at your option) any later version.
*
*	This program is distributed in the hope that it will be useful,
*	but WITHOUT ANY WARRANTY; without even the implied warranty of
*	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
*	GNU General Public License for more details.
*
*	You should have received a copy of the GNU General Public License
*	along with this program.If not, see <http://www.gnu.org/licenses/>.
*/


#include "CudaBuffer.h"
#include "../blocks/cuda/SWE_BlockCUDA.hh"


/**
 * creats two Arrays on the device
 * @param w number of cells in x direction
 * @param h number of cells in y direction
 */
CudaBuffer::CudaBuffer(int w, int h) {
	width = w;
	height = h;

	int size = (width) * (height) * sizeof(float);
	//allocate CUDA memory for h and b
	cudaMalloc((void**) &hd, size);
		checkCUDAError("allocate device memory for h");
	cudaMalloc((void**) &bd, size);
		checkCUDAError("allocate device memory for bd");
}

CudaBuffer::~CudaBuffer(){
	cudaFree(hd);
	cudaFree(bd);
}

/**
 * copies the data from the two input Float2D Arrays onto the device
 * @param h Float2D containing the waterheight
 * @param b Float2D containing the bathymetry
 */
void CudaBuffer::copyToBuffer(Float2D *h, Float2D *b){
	//printf("rows: %d, cols: %d, sum: %d, w: %d, h: %d, w*h: %d\n", h->getRows(), h->getCols(),
	//		h->getRows()*h->getCols(), width, height, width*height);

	cudaMemcpy(hd, &h->elemVector()[0], width*height*sizeof(float), cudaMemcpyHostToDevice);
		checkCUDAError("copying h to Device");

	//printf("copy h done\n");

	cudaMemcpy(bd, &b->elemVector()[0], width*height*sizeof(float), cudaMemcpyHostToDevice);
		checkCUDAError("copying b to Device");

	//printf("copy b done\n");
}
