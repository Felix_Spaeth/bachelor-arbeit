/*  MPI_Frave provides MPI for the SWE_Frave Program
*
*	Copyright(C) 2015  Felix Spaeth
*
*	This program is free software : you can redistribute it and / or modify
*	it under the terms of the GNU General Public License as published by
*	the Free Software Foundation, either version 3 of the License, or
*	(at your option) any later version.
*
*	This program is distributed in the hope that it will be useful,
*	but WITHOUT ANY WARRANTY; without even the implied warranty of
*	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
*	GNU General Public License for more details.
*
*	You should have received a copy of the GNU General Public License
*	along with this program.If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _MPI_FRAVE
#define _MPI_FRAVE

#include <stdio.h>
#include <stdlib.h>
#include <cmath>
#include <string>
#include <stddef.h>

#include <mpi.h>
#include <cuda_runtime.h>

#include "../blocks/cuda/SWE_WavePropagationBlockCuda.hh"
#include "../tools/Logger.hh"
#include "../blocks/cuda/SWE_BlockCUDA.hh"


struct Point{
	int x, y;
	float b;
};

struct PointForMPI{
	int x, y;
	float h, b, time;
};

class MPI_FRAVE{

	public:
		MPI_FRAVE(int numScenarios);
		~MPI_FRAVE();

		/**
		 * Initializes the MPI Communication
		 * and sets the rank of the graph
		 * @param argc
		 * @param argv
		 */
		void init(int *argc, char* argv[]);

		/**
		 * closes the MPI Connection
		 */
		void finalize();

		/**
		 * terminates the Program
		 */
		void abort();

		/**
		 * guarantees that all Processes use the same width and height for computation
		 */
		void setSize(int &w, int &h);

		float maxTimeStep(float l_maxTimeStepWidth);

		/**
		 * Exchanges the left and right ghost layers with MPI's SendReceive.
		 *
		 * @param o_leftInflow ghost layer, where the left neighbor writes into.
		 * @param i_leftOutflow layer where the left neighbor reads from.
		 * @param o_rightInflow ghost layer, where the right neighbor writes into.
		 * @param i_rightOutflow layer, where the right neighbor reads form.
		 */
		void exchangeLeftRightGhostLayers( SWE_Block1D* o_leftInflow,  SWE_Block1D* i_leftOutflow,
										   SWE_Block1D* o_rightInflow, SWE_Block1D* i_rightOutflow);

		/**
		 * Exchanges the bottom and top ghost layers with MPI's SendReceive.
		 *
		 * @param o_bottomNeighborInflow ghost layer, where the bottom neighbor writes into.
		 * @param i_bottomNeighborOutflow host layer, where the bottom neighbor reads from.
		 * @param o_topNeighborInflow ghost layer, where the top neighbor writes into.
		 * @param i_topNeighborOutflow ghost layer, where the top neighbor reads from.
		 */
		void exchangeBottomTopGhostLayers( SWE_Block1D* o_bottomNeighborInflow, SWE_Block1D* i_bottomNeighborOutflow,
										   SWE_Block1D* o_topNeighborInflow,    SWE_Block1D* i_topNeighborOutflow);

		/**
		 * Exchanges the status messages to handle user interaction
		 * @param doReset gets set true for restart od the current scenario or a new scenario
		 * @param stop currently unused
		 * @param newScenario gets set true if a new scenario should be started
		 * @param scenario number of the next scenario
		 */
		void exchangeStatus(bool &doReset, bool &stop, bool &newScenario, int &scenario);

		/**
		 * @return number of Block Rows
		 */
		int computeNumberOfBlockRows();

		/**
		 * returns the current MPI-Rank as if there were no Graph-classes
		 */
		int getMpiRank();
		/**
		 * returns the number of non Graph processes
		 */
		int getNumberOfProcesses();

		/**
		 * sends the Data of all registered Points
		 * @param d_h Pointer to the device address where the height data is
		 * @param d_b Pointer to the device address where the bathymetry data is
		 * @param time current time of simulation
		 */
		void transmitPoints(const float* d_h, const float* b, float time);

		int getNumPoints(){return numPoints;};
		const Point* getPoints(){return points;};

		//! local position of this MPI process in x- and y-direction.
		int blockPositionX, blockPositionY;
		//! number of SWE_Blocks in x- and y-direction.
		int blocksX, blocksY;



	private:
		/**
		 * Transmits a single Point
		 * @param x Koordinate of the Point
		 * @param y Koordinate of the Point
		 * @param d_h d_h Pointer to the device address where the height data is
		 * @param b bathymetry value for this Point
		 * @param time current time of simulation
		 */
		void transmitPoint(int x, int y, const float *d_h, float d_b, float time);

		/**
		 * a new Point is registered
		 */
		void registerPoint();

		/**
		* @param i rank of MPI-neighbor without an Graph Process
		* returns the MPI-Rank of neighbor with Graph Process
		*/
		int getNeighbor(int i);


		//! MPI Rank of a process.
		int mpiRank;
		//! number of MPI processes.
		int numberOfProcesses;
		//! MPI ranks of the neighbors
		int leftNeighborRank, rightNeighborRank, bottomNeighborRank, topNeighborRank;

		MPI_Datatype mpiRow;
		//! MPI row-vector: 1 block, l_nYLocal+2 elements per block, stride of 1
		MPI_Datatype mpiCol;
		MPI_Datatype mpiPoint;
		//MPI_Comm commWithoutGraph;

		int width, height;
		int num_of_scenarios;
		bool RowSet;
		bool ColSet;

		bool graphExists;
		int rankOfGraph;

		int numPoints;
		Point* points=NULL;
};

#endif /*_MPI_FRAVE*/
