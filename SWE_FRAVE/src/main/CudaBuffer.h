/* Copies the data of float2D arrays to the GPU
*
*	Copyright(C) 2015  Felix Spaeth
*
*	This program is free software : you can redistribute it and / or modify
*	it under the terms of the GNU General Public License as published by
*	the Free Software Foundation, either version 3 of the License, or
*	(at your option) any later version.
*
*	This program is distributed in the hope that it will be useful,
*	but WITHOUT ANY WARRANTY; without even the implied warranty of
*	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
*	GNU General Public License for more details.
*
*	You should have received a copy of the GNU General Public License
*	along with this program.If not, see <http://www.gnu.org/licenses/>.
*/

#include <cuda_runtime.h>
#include "../tools/help.hh"

class CudaBuffer {
public:

	/**
	 * creats two Arrays on the device
	 * @param w number of cells in x direction
	 * @param h number of cells in y direction
	 */
	CudaBuffer(int w, int h);
	~CudaBuffer();

	/**
	 * @return pointer to the Array with the waterheight
	 */
	const float* getCUDA_waterHeight(){
		return hd;
	};

	/**
	 * @return pointer to the Array with the bathymetry
	 */
	const float* getCUDA_bathymetry() {
		return bd;
	};

	/**
	 * copies the data from the two input Float2D Arrays onto the device
	 * @param h Float2D containing the waterheight
	 * @param b Float2D containing the bathymetry
	 */
	void copyToBuffer(Float2D *h, Float2D *b);
private:
	float *hd=NULL;
	float *bd=NULL;
	int width, height;
};
