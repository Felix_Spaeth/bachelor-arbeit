/* fills a Buffer with the Texture color informations
*
*	Copyright(C) 2015  Felix Spaeth
*
*	This program is free software : you can redistribute it and / or modify
*	it under the terms of the GNU General Public License as published by
*	the Free Software Foundation, either version 3 of the License, or
*	(at your option) any later version.
*
*	This program is distributed in the hope that it will be useful,
*	but WITHOUT ANY WARRANTY; without even the implied warranty of
*	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
*	GNU General Public License for more details.
*
*	You should have received a copy of the GNU General Public License
*	along with this program.If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <stdlib.h>



/**
 * CUDA kernel function that fills the d_o_color field with the corresponding color data
 * RGBA -- Red Green Blue Alpha
 */
__global__ void draw_C(unsigned char * d_o_color, int size, const float * d_h, const float * d_b, float min_h, float max_h) {
	//unsigned char *idata = (unsigned char*) d_o_color;
	int i = blockDim.x * blockIdx.x + threadIdx.x;
	if(i<size){

		//Not a Number becomes magenta
		if(d_h[i]!=d_h[i]){
			d_o_color[4*i] = 255;
			d_o_color[4*i+1] = 0;
			d_o_color[4*i+2] = 255;
			d_o_color[4*i+3] = 255;


		}else{
			if(d_b[i]>0){ //land
				// https://de.wikipedia.org/wiki/Regionalfarbe
				// combined with
				// http://www.farb-tabelle.de/en/table-of-color.htm

				if(d_b[i]<100){
					d_o_color[4*i] = 144;   //red
					d_o_color[4*i+1] = 238;	//green
					d_o_color[4*i+2] = 144;	//blue
					d_o_color[4*i+3] = 255; //alpha
				}else if(d_b[i]<200){
					d_o_color[4*i] = 173;
					d_o_color[4*i+1] = 255;
					d_o_color[4*i+2] = 47;
					d_o_color[4*i+3] = 255;
				}else if(d_b[i]<500){
					d_o_color[4*i] = 255;
					d_o_color[4*i+1] = 193;
					d_o_color[4*i+2] = 37;
					d_o_color[4*i+3] = 255;
				}else if(d_b[i]<1000){
					d_o_color[4*i] = 205;
					d_o_color[4*i+1] = 155;
					d_o_color[4*i+2] = 29;
					d_o_color[4*i+3] = 255;
				}
				else if (d_b[i] < 2000){
					d_o_color[4 * i] = 205;
					d_o_color[4 * i + 1] = 102;
					d_o_color[4 * i + 2] = 0;
					d_o_color[4 * i + 3] = 255;
				}
				else if (d_b[i] < 4000){
					d_o_color[4*i] = 139;
					d_o_color[4*i+1] = 69;
					d_o_color[4*i+2] = 19;
					d_o_color[4*i+3] = 255;
				}else{
					d_o_color[4*i] = 255;
					d_o_color[4*i+1] = 255;
					d_o_color[4*i+2] = 255;
					d_o_color[4*i+3] = 255;
				}

			}else{ //water
				//inspired by: http://stackoverflow.com/questions/7706339/grayscale-to-red-green-blue-matlab-jet-color-scale

				//blue-white-red
				float height=d_h[i]+d_b[i];
				float zo=(height-min_h)/(max_h-min_h);//from zero to one
				if(zo<=0.5){	//from 0 to 0.5 
								//black to blue
					zo=zo*2;

					d_o_color[4*i] = (unsigned char) 0;    
					d_o_color[4*i+1] = (unsigned char) 0;
					d_o_color[4*i+2] = (unsigned char) (zo * 255);
					d_o_color[4*i+3] = (unsigned char) 255;


				}else if(zo<0.75){  //from 0.5 to 0.75
									//blue to white
					zo=(zo-0.5)*4;

					d_o_color[4*i] = (unsigned char) (zo * 255);    
					d_o_color[4*i+1] = (unsigned char) (zo * 255);
					d_o_color[4*i+2] = (unsigned char) 255;
					d_o_color[4*i+3] = (unsigned char) 255;
				}else{
									//from 0.75 to 1
									//white to red
					zo=(zo-0.75)*4;

					d_o_color[4*i] = (unsigned char) 255;
					d_o_color[4*i+1] = (unsigned char) (255-zo * 255);
					d_o_color[4*i+2] = (unsigned char) (255-zo *255);
					d_o_color[4*i+3] = (unsigned char) 255;
				}




			}
		}

	}
}


/**
* Cuda kernel Funktion that fills a single Point with yellow
*/
__global__ void draw_Point_C(unsigned char *d_o_color, int pos1D){
	//red+green=yellow
	d_o_color[4*pos1D] = (unsigned char) 255;  //r
	d_o_color[4*pos1D+1] = (unsigned char) 255;//g
	d_o_color[4*pos1D+2] = (unsigned char) 0;  //b
	d_o_color[4*pos1D+3] = (unsigned char) 255;//a
}


/**
 * Fills the buffer the d_output_ptr Pointer points to with the color values
 * @param d_output_ptr output: contains the color informations
 * @param width of the 2D Array
 * @param height oh the 2D Array
 * @param d_h Pointer to the device memory containing h
 * @param d_b Pointer to the device memory containing b
 * @param min Waterheight to color
 * @param max Waterheight to color
 */
void fill(void* d_output_ptr, int width, int height, const float * d_h, const float * d_b, float min, float max){

	unsigned char *d_ptr = (unsigned char*) d_output_ptr;
	//int blockSize=128;
	int size=width*height;
    //int numBlocks = (size + blockSize-1) / blockSize;


	int blockSize;      // The launch configurator returned block size
	int minGridSize;    // The minimum grid size needed to achieve the maximum occupancy for a full device launch
	int gridSize;       // The actual grid size needed, based on input size

	cudaOccupancyMaxPotentialBlockSize(&minGridSize, &blockSize, (void*)draw_C, 0, size);
	gridSize = (size+1 + blockSize - 1) / blockSize;

	//draw_C<<<numBlocks, blockSize>>>(d_ptr, size, d_h, d_b, min_h, max_h);
	draw_C<<<gridSize, blockSize>>>(d_ptr, size, d_h, d_b, min, max);

}

/**
 * Compute the position of 2D coordinates in a 1D array.
 *   array[i][j] -> i * ny + j
 *
 * @param i_i row index.
 * @param i_j column index.
 * @param i_ny #(cells in y-direction).
 * @return 1D index.
 */
inline int computeOneDPositionKernel(const int row, const int col, const int i_ny) {
  return row*i_ny + col;
}

/**
 * fills the Pixel at the Position in yellow
 * @param d_output_ptr output: contains the color informations
 * @param x Poisition of Point
 * @param y Poisition of Point
 * @param height number of cells in Y direction of the SWE_Block
 */
void drawPoint(void *d_output_ptr, int x, int y, int height){

	unsigned char *d_ptr = (unsigned char*) d_output_ptr;
	int pos1D = computeOneDPositionKernel(x+1, y+1, height);
	draw_Point_C<<<1, 1>>>(d_ptr, pos1D);


}

/**
 * waits till completion of all CUDA funktions 
 */
void synchronize(){
	cudaDeviceSynchronize();
}


