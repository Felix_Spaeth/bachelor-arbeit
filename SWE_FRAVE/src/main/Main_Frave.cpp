/* Main part of program to simulate Tsunamis
*
*	Copyright(C) 2015  Felix Spaeth
*
*	This program is free software : you can redistribute it and / or modify
*	it under the terms of the GNU General Public License as published by
*	the Free Software Foundation, either version 3 of the License, or
*	(at your option) any later version.
*
*	This program is distributed in the hope that it will be useful,
*	but WITHOUT ANY WARRANTY; without even the implied warranty of
*	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
*	GNU General Public License for more details.
*
*	You should have received a copy of the GNU General Public License
*	along with this program.If not, see <http://www.gnu.org/licenses/>.
*/


#include "Main_Frave.h"
#define DBG
#define NUM_OF_SCENARIOS 5
#define MAX_H 1.0f

int screen_width=1366, screen_height=768;

GLuint program;
GLuint texture_id;
GLuint buffer_id;

cudaGraphicsResource *resources[1];
cudaStream_t cuda_stream;

MPI_FRAVE *mpi=NULL;

void *d_buffer;

int width=256, height=256;//16*n
int l_nX, l_nY;

//time for frame count
float timeSec = 0;
float lasttime = 0;
int framecount = 0;

#ifdef CUDA_WAVEPROP
SWE_WavePropagationBlockCuda *l_wavePropagationBlock=NULL;
#else
SWE_WavePropagationBlock *l_wavePropagationBlock=NULL;
CudaBuffer *cuB=NULL;;
#endif

int scenario=0;
//simulated time since last reset
float simTime=0.0;

SWE_Block1D *l_leftInflow=NULL, *l_leftOutflow=NULL,
		*l_rightInflow=NULL, *l_rightOutflow=NULL,
		*l_bottomInflow=NULL, *l_bottomOutflow=NULL,
		*l_topInflow=NULL, *l_topOutflow=NULL;


bool doReset=false;
bool stop=false;
bool isFrave=false;
bool newScenario=false;

float min_h = -MAX_H, max_h = MAX_H;


/**
* Creats the OpenGL Buffer and Texture and calls reset()
*/
int init_resources() {

	//pixel buffer object
	glGenBuffers(1, &buffer_id);
	glBindBuffer(GL_PIXEL_UNPACK_BUFFER, buffer_id);
	glBufferData(GL_PIXEL_UNPACK_BUFFER, (height+2)*(width+2)*4*sizeof(unsigned char), NULL, GL_DYNAMIC_DRAW);

	//Create Texture as 2D Image
	glGenTextures(1, &texture_id);
	glBindTexture(GL_TEXTURE_2D, texture_id);
	glTexImage2D(GL_TEXTURE_2D,
			0,
			GL_RGBA8,
			height+2,
			width+2,
			0,
			GL_RGBA,
			GL_UNSIGNED_BYTE,
			NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glBindBuffer(GL_PIXEL_UNPACK_BUFFER, 0);
	glBindTexture(GL_TEXTURE_2D, 0);

	
	cudaGraphicsGLRegisterBuffer(resources, buffer_id, cudaGraphicsRegisterFlagsWriteDiscard);  //registers buffer as CUDA graphics ressource as read only
	cudaStreamCreate(&cuda_stream);
	cudaGraphicsMapResources(1, resources, cuda_stream);
	size_t size;
	cudaGraphicsResourceGetMappedPointer((void **)(&d_buffer), &size, resources[0]);
	cudaGraphicsUnmapResources(1, resources, cuda_stream);
	cudaStreamDestroy(cuda_stream);

	program = glCreateProgram();

	/*-----------init SWE---------------*/
	reset();
	/*-----------end init SWE---------------*/


	return 1;
}


/**
* Method used for creation of the first Scenario and
* used to reset the current Scenario or change the Scenario
*/
void reset(){

	doReset=false;
	simTime=0;

	//printf("Rank: %d, numProcesses: %d\n", mpi->getMpiRank(), mpi->getNumberOfProcesses());

	l_nX=width*mpi->blocksX;
	l_nY=height*mpi->blocksY;

	tools::Logger::logger.printNumberOfCells(l_nX, l_nY);
	tools::Logger::logger.printNumberOfBlocks(mpi->blocksX, mpi->blocksY);
	SWE_Scenario* l_scenario=NULL;
	// create a scenario
	switch(scenario){
		case 0: l_scenario = new SWE_RadialDamBreakScenario();
				break;
		case 1: l_scenario = new SWE_SideDamBrake();
				break;
		case 2: l_scenario = new SWE_BathymetryDamBreakScenario();
				break;
		case 3: l_scenario = new SWE_LeftDamBreak();
				break;
		case 4: l_scenario = new SWE_TsunamiScenario("chile_gebco_usgs_2000m_bath.nc", "chile_gebco_usgs_2000m_displ.nc");
				break;
		case 5: l_scenario = new SWE_TsunamiScenario("tohoku_gebco_ucsb3_2000m_hawaii_bath.nc", "tohoku_gebco_ucsb3_2000m_hawaii_displ.nc");
						break;
	}


	//! size of a single cell in x- and y-direction
	float l_dX, l_dY;

	// compute the size of a single cell
	l_dX = (l_scenario->getBoundaryPos(BND_RIGHT) - l_scenario->getBoundaryPos(BND_LEFT) )/(l_nX+2); //+2 because of ghost layer
	l_dY = (l_scenario->getBoundaryPos(BND_TOP) - l_scenario->getBoundaryPos(BND_BOTTOM) )/(l_nY+2); //+2 because of ghost layer

	tools::Logger::logger.printCellSize(l_dX, l_dY);
	tools::Logger::logger.printNumberOfCellsPerProcess(width, height);

	if(l_wavePropagationBlock!=NULL){
		delete l_wavePropagationBlock; //delete old WavePropagationBlock
	}
#ifdef CUDA_WAVEPROP
	l_wavePropagationBlock= new SWE_WavePropagationBlockCuda(width, height, l_dX,l_dY);
#else
	if(cuB==NULL)
		cuB=new CudaBuffer(height+2, width+2);
	l_wavePropagationBlock= new SWE_WavePropagationBlock(width, height, l_dX,l_dY);
#endif

	//! origin of the simulation domain in x- and y-direction
	float l_originX, l_originY;

	// get the origin from the scenario
	l_originX = l_scenario->getBoundaryPos(BND_LEFT) + mpi->blockPositionX*width*l_dX;
	l_originY = l_scenario->getBoundaryPos(BND_BOTTOM) + mpi->blockPositionY*height*l_dY;

	// initialize the wave propagation block
	l_wavePropagationBlock->initScenario(l_originX, l_originY, *l_scenario, true);


	/*
	* Connect SWE blocks at boundaries
	*/
	// left and right boundaries
	tools::Logger::logger.printString("Connecting SWE blocks at left boundaries.");
	l_leftInflow  = l_wavePropagationBlock->grabGhostLayer(BND_LEFT);
	l_leftOutflow = l_wavePropagationBlock->registerCopyLayer(BND_LEFT);
	if (mpi->blockPositionX == 0)
		l_wavePropagationBlock->setBoundaryType(BND_LEFT, l_scenario->getBoundaryType(BND_LEFT));

	tools::Logger::logger.printString("Connecting SWE blocks at right boundaries.");
	l_rightInflow  = l_wavePropagationBlock->grabGhostLayer(BND_RIGHT);
	l_rightOutflow = l_wavePropagationBlock->registerCopyLayer(BND_RIGHT);
	if (mpi->blockPositionX == mpi->blocksX-1)
		l_wavePropagationBlock->setBoundaryType(BND_RIGHT, l_scenario->getBoundaryType(BND_RIGHT));

	// bottom and top boundaries
	tools::Logger::logger.printString("Connecting SWE blocks at bottom boundaries.");
	l_bottomInflow  = l_wavePropagationBlock->grabGhostLayer(BND_BOTTOM);
	l_bottomOutflow = l_wavePropagationBlock->registerCopyLayer(BND_BOTTOM);
	if (mpi->blockPositionY == 0)
		l_wavePropagationBlock->setBoundaryType(BND_BOTTOM, l_scenario->getBoundaryType(BND_BOTTOM));

	tools::Logger::logger.printString("Connecting SWE blocks at top boundaries.");
	l_topInflow  = l_wavePropagationBlock->grabGhostLayer(BND_TOP);
	l_topOutflow = l_wavePropagationBlock->registerCopyLayer(BND_TOP);
	if (mpi->blockPositionY == mpi->blocksY-1)
		l_wavePropagationBlock->setBoundaryType(BND_TOP, l_scenario->getBoundaryType(BND_TOP));


	// intially exchange ghost and copy layers
	mpi->exchangeLeftRightGhostLayers( l_leftInflow,  l_leftOutflow,
				  l_rightInflow, l_rightOutflow );

	tools::Logger::logger.printString("LeftRight exchanged.");


	mpi->exchangeBottomTopGhostLayers( l_bottomInflow, l_bottomOutflow,
				  l_topInflow, l_topOutflow );
	tools::Logger::logger.printString("TopBottom exchanged.");

    // set values in ghost cells:
    l_wavePropagationBlock->setGhostLayer();

    delete l_scenario;
}


/**
* Callback-method called in every Main-Loop Cycle. 
* Used for simulating one timeStep and generating the Texture
*/
void onIdle() {

	float current = glutGet(GLUT_ELAPSED_TIME);
	if(mpi->getMpiRank()==0){
		timeSec += current - lasttime;
		lasttime = current;
		framecount++;
		if (timeSec / 1000.0f >= 1.0f) {
			printf("Frames per sec: %d\n", framecount);
			timeSec = 0.0f;
			framecount = 0;
		}
	}
	/*-----------------------Begin MPI --------------------------*/

	//exchanges the status between all mpi-Processes
	mpi->exchangeStatus(doReset, stop, newScenario, scenario);

	if(doReset){
		printf("------------RESET---------------------\n");
		reset();
	}


	// exchange ghost and copy layers
	mpi->exchangeLeftRightGhostLayers(l_leftInflow, l_leftOutflow, l_rightInflow, l_rightOutflow);

	//tools::Logger::logger.printString("LeftRight exchanged.");

	mpi->exchangeBottomTopGhostLayers(l_bottomInflow, l_bottomOutflow, l_topInflow, l_topOutflow);
	//tools::Logger::logger.printString("TopBottom exchanged.");

	// set values in ghost cells:
	l_wavePropagationBlock->setGhostLayer();

	/*-----------------------End MPI --------------------------*/


	/* ----------------------Begin SWE LOOP---------------------------------------*/



    // compute numerical flux on each edge
    l_wavePropagationBlock->computeNumericalFluxes();
     //! maximum allowed time step height within a block.
    float l_maxTimeStepWidth = l_wavePropagationBlock->getMaxTimestep();

    //! maximum allowed time steps of all blocks
    float l_maxTimeStepWidthGlobal= mpi->maxTimeStep(l_maxTimeStepWidth);
    simTime+=l_maxTimeStepWidthGlobal;

    l_wavePropagationBlock->updateUnknowns(l_maxTimeStepWidthGlobal);


    /*--------------------End SWE LOOP----------------*/

	cudaStreamCreate(&cuda_stream);
	cudaGraphicsMapResources(1, resources, cuda_stream);

#ifdef CUDA_WAVEPROP
	fill(d_buffer, width+2, height+2,  l_wavePropagationBlock->getCUDA_waterHeight(), l_wavePropagationBlock->getCUDA_bathymetry(), min_h, max_h);
	mpi->transmitPoints(l_wavePropagationBlock->getCUDA_waterHeight(), l_wavePropagationBlock->getCUDA_bathymetry(), simTime);
#else
	cuB->copyToBuffer(&l_wavePropagationBlock->getWaterHeight(), &l_wavePropagationBlock->getBathymetry());
	fill(d_buffer, width + 2, height + 2,  cuB->getCUDA_waterHeight(), cuB->getCUDA_bathymetry(), min_h, max_h);
	mpi->transmitPoints(cuB->getCUDA_waterHeight(), cuB->getCUDA_bathymetry(), simTime);
#endif
	synchronize();

	for(int i=0;i<(mpi->getNumPoints());i++){
		drawPoint(d_buffer,  mpi->getPoints()[i].x, mpi->getPoints()[i].y,  height+2);
	}

	cudaGraphicsUnmapResources(1, resources, cuda_stream);
	cudaStreamDestroy(cuda_stream);

	glutPostRedisplay();


}


/**
* Callback-methode, used to diplay the Texture
*/
void onDisplay() {

	glUseProgram(program);


	glBindBuffer(GL_PIXEL_UNPACK_BUFFER, buffer_id);
	glBindTexture(GL_TEXTURE_2D, texture_id);

	glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, height+2, width+2,  GL_RGBA, GL_UNSIGNED_BYTE, NULL);
	glEnable(GL_TEXTURE_2D);

	glBegin(GL_QUADS);
		glTexCoord2f(0.0, 1.0); glVertex2f(1.0, -1.0);
		glTexCoord2f(1.0, 1.0); glVertex2f(1.0, 1.0);
		glTexCoord2f(1.0, 0.0); glVertex2f(-1.0, 1.0);
		glTexCoord2f(0.0, 0.0); glVertex2f(-1.0, -1.0);
	glEnd();

	glDisable(GL_TEXTURE_2D);
	glBindBuffer(GL_PIXEL_UNPACK_BUFFER, 0);
	glBindTexture(GL_TEXTURE_2D, 0);

	glutSwapBuffers();
}

/**
*	Callback Methode that defines the behavior when the windows size is changed
*/
void onReshape(int _width, int _height) {
  screen_width = _width;
  screen_height = _height;
  glViewport(0, 0, screen_width, screen_height);
}

/**
*	Callback Methode used to react on user input
*/
void onKey(unsigned char key, int x, int y){
	printf("%c", key);

	switch(key){
		case 'r':
		case 'R':
			doReset=true;
			break;
		case 's':
		case 'S':
			stop=true;
			break;
		case '1':
			scenario=0;
			newScenario=true;
			break;
		case '2':
			scenario=1;
			newScenario=true;
			break;
		case '3':
			scenario=2;
			newScenario=true;
			break;
		case '4':
			scenario=3;
			newScenario=true;
			break;
		case '5':
			scenario=4;
			newScenario=true;
			break;
		case '6':
			scenario=5;
			newScenario=true;
			break;
		case ' ':
			printf("mpiRank: %d, numProcesses: %d\n", mpi->getMpiRank(), mpi->getNumberOfProcesses());
			printf("blocksX: %d, blocksY: %d\n", mpi->blocksX, mpi->blocksY);
			printf("posX: %d, posY: %d\n", mpi->blockPositionX, mpi->blockPositionY);
			printf("lnX: %d, lnY: %d\n", l_nX, l_nY);

	}
}

/**
* releases all used Memory by destroying all Objects
*/
void free_resources() {

	glDeleteProgram(program);

	glDeleteBuffers(1, &buffer_id);
	glDeleteTextures(1, &texture_id);
	delete l_wavePropagationBlock;
#ifndef CUDA_WAVEPROP
	delete cuB;
#endif
	mpi->finalize();
}



int main(int argc, char* argv[]) {


	/**
	* Initialization.
	*/

	mpi=new MPI_FRAVE(NUM_OF_SCENARIOS);
	mpi->init(&argc, argv);
	glutInit(&argc, argv);



	tools::Args args;
	args.addOption("grid-size-x", 'x', "Number of cells in x direction per Process", tools::Args::Required, false);
	args.addOption("grid-size-y", 'y', "Number of cells in y direction per Process", tools::Args::Required, false);
	args.addOption("maxH", 'm', "Maximum waterheight for colormap, minH = -maxH", tools::Args::Required, false);

	args.addOption("frave", 'f', "Activate if frave is used", tools::Args::No, false);
	args.addOption("device", 'd', "GPU used for CUDA and OpenGL + opens the window in fullscreen", tools::Args::Required, false);

	tools::Args::Result ret = args.parse(argc, argv, mpi->getMpiRank() == 0);

	switch (ret){
		case tools::Args::Error:
			mpi->abort();
			return 1;
		case tools::Args::Help:
			mpi->finalize();
			return 0;
		default:;
	}
	isFrave = args.isSet("frave");

	width = args.getArgument<int>("grid-size-x", width);
	height = args.getArgument<int>("grid-size-y", height);
	//printf("height: %d, width: %d\n", height, width);
	mpi->setSize(width, height);
	printf("width2: %d, height2: %d\n", width, height);

	max_h = args.getArgument<float>("maxH", MAX_H); 
	min_h = -max_h;

	
	//setup CUDA Context
	if(!isFrave){
		if(!args.isSet("device")){
			int l_cudaDevicesPerNode = 1;
		

			//! the id of the node local GPU
			int l_cudaDeviceId = mpi->getMpiRank() % l_cudaDevicesPerNode;
			cudaGLSetGLDevice(l_cudaDeviceId);
		}else{
			cudaGLSetGLDevice(args.getArgument<int>("device", 0));
		}
	}else{
		int dev=0;
		switch (mpi->getMpiRank()){
			case 0: dev = 0;
					break;
			case 1: dev = 2;
					break;
			case 2: dev = 0;
					break;
			case 3: dev = 1;
					break;
			case 4: dev = 0;
					break;
			case 5: dev = 2;
					break;
		}
		cudaGLSetGLDevice(dev);

	}



	//create Window using glut
	glutInitContextVersion(2, 0);
	glutInitDisplayMode(GLUT_ALPHA | GLUT_DOUBLE);
	if(!isFrave && !args.isSet("device")){
		glutInitWindowSize((screen_width-150)/mpi->blocksX, (screen_height-100)/mpi->blocksY);
//		glutInitWindowPosition(70+(((screen_width-70)/mpi->blocksY)-10)*mpi->blockPositionY, 
//			20+((screen_height-100)/mpi->blocksX)*(mpi->blocksX-mpi->blockPositionX-1));
		glutInitWindowPosition(70+(((screen_width-70)/mpi->blocksX)-10)*mpi->blockPositionX, 
			20+((screen_height+40)/mpi->blocksY)*(mpi->blocksY-mpi->blockPositionY-1));

	}//else{
		//FullScreen in der FRAVE

	//}
	
	std::ostringstream os ;
	os << "Tsunami Simulation: " << mpi->getMpiRank() ;
	glutCreateWindow(os.str().c_str());
	if(isFrave || args.isSet("device"))glutFullScreen();

	GLenum glew_status = glewInit();
	if (glew_status != GLEW_OK) {
		fprintf(stderr, "Error: %s\n", glewGetErrorString(glew_status));
		return 1;
	}

	if (!GLEW_VERSION_2_0 ) {
		fprintf(stderr,
				"Error: your graphic card does not support OpenGL 2.0\n");
		return 1;
	}


	if (init_resources()) {
		glutDisplayFunc(onDisplay);
	    glutReshapeFunc(onReshape);
		glutIdleFunc(onIdle);
		glutKeyboardFunc(onKey);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glutMainLoop();
	}

	free_resources();
	cudaDeviceReset();
	return 0;
}
