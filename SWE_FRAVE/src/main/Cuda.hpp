/* fills a Buffer with the Texture color informations
*
*	Copyright(C) 2015  Felix Spaeth
*
*	This program is free software : you can redistribute it and / or modify
*	it under the terms of the GNU General Public License as published by
*	the Free Software Foundation, either version 3 of the License, or
*	(at your option) any later version.
*
*	This program is distributed in the hope that it will be useful,
*	but WITHOUT ANY WARRANTY; without even the implied warranty of
*	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
*	GNU General Public License for more details.
*
*	You should have received a copy of the GNU General Public License
*	along with this program.If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef C_MAIN_HPP_
#define C_MAIN_HPP_
#include "MPI_Frave.h"


/**
 * CUDA kernel function that fills the d_o_color field with the corresponding color data
 * RGBA -- Red Green Blue Alpha
 */
__global__ void draw_C(unsigned char *d_o_color, int size, const float * d_h, const float * d_b, float min_h, float max_h);


/**
* Cuda kernel Funktion that fills a single Point with yellow
*/
__global__ void draw_Point_C(unsigned char *d_o_color, int pos1D);

/**
 * Fills the buffer the d_output_ptr Pointer points to with the color values
 * @param d_output_ptr output: contains the color informations
 * @param width of the 2D Array
 * @param height oh the 2D Array
 * @param d_h Pointer to the device memory containing h
 * @param d_b Pointer to the device memory containing b
 * @param min Waterheight to color
 * @param max Waterheight to color
 */
void fill(void *d_output_ptr, int width, int height, const float* d_h, const float* d_b, float min, float max);

/**
 * fills the Pixel at the Position in yellow
 * @param d_output_ptr output: contains the color informations
 * @param x Poisition of Point
 * @param y Poisition of Point
 * @param height number of cells in Y direction of the SWE_Block
 */
void drawPoint(void *d_output_ptr, int x, int y, int height);


/**
 * waits till completion of all CUDA funktions
 */
void synchronize();


#endif /* C_MAIN_HPP_ */
