/* visualizes Tsunamis on one or more Monitors 
 *
 *	Copyright(C) 2015  Felix Spaeth
 *
 *	This program is free software : you can redistribute it and / or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	This program is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with this program.If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef MAIN_FRAVE
#define MAIN_FRAVE

#define CUDA_WAVEPROP  //remove this line to use CPU computation for the Tsunamis calculation


#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string>
#include<sstream>
//#include <vector>



/* Use glew.h instead of gl.h to get all the GL prototypes declared */
#include <GL/glew.h>
/* Using the GLUT library for the base windowing setup */
#include <GL/freeglut.h>

/*CUDA*/
//#include <cudagl.h>
#include <cuda_gl_interop.h>

#include "Cuda.hpp"

#ifndef CUDA_WAVEPROP
#include "CudaBuffer.h"
#endif

#include "MPI_Frave.h"
#include "../blocks/cuda/SWE_WavePropagationBlockCuda.hh"
#include "../blocks/SWE_WavePropagationBlock.hh"
#include "../scenarios/SWE_simple_scenarios.hh"
#include "../scenarios/SWE_TsunamiScenario.hh"
#include "../tools/Logger.hh"
#include "../tools/args.hh"
 

/**
* Creats the OpenGL Buffer and Texture and calls reset()
*/
int init_resources();

/**
* Method used for creation of the first Scenario and
* used to reset the current Scenario or change the Scenario
*/
void reset();

/**
* Callback-method called in every Main-Loop Cycle.
* Used for simulating one timeStep and generating the Texture
*/
void onIdle();

/**
* Callback-methode, used to diplay the Texture
*/
void onDisplay();


/**
*	Callback Methode that defines the behavior when the windows size is changed
*/
void onReshape(int width, int height);

/**
*	Callback Methode used to react on user input
*/
void onKey(unsigned char key, int x, int y);

/**
* releases all used Memory by destroying all Objects
*/
void free_resources();


int main(int argc, char* argv[]);


#endif /* MAIN_FRAVE */
