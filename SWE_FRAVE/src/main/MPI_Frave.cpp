#include "MPI_Frave.h"
#define SWE -1

#define RUNNING 100
#define RESET 101
#define STOP 102
#define REGISTER_POINT 103
#define POINT_TAG_OLD 2021


/**
 * Compute the position of 2D coordinates in a 1D array.
 *   array[i][j] -> i * ny + j
 *
 * @param i_i row index.
 * @param i_j column index.
 * @param i_ny #(cells in y-direction).
 * @return 1D index.
 */
inline int computeOneDPositionKernel(const int row, const int col, const int i_ny) {
  return row*i_ny + col;
}



MPI_FRAVE::MPI_FRAVE(int numScenarios) :
		num_of_scenarios(numScenarios), RowSet(false), ColSet(false),
				rankOfGraph(-1),  numPoints(0){

}

MPI_FRAVE::~MPI_FRAVE() {
	free(points);
	MPI_Type_free(&mpiCol);
	MPI_Type_free(&mpiRow);
	MPI_Type_free(&mpiPoint);
}

/**
 * Initializes the MPI Communication
 * and sets the rank of the graph
 * @param argc
 * @param argv
 */
void MPI_FRAVE::init(int *argc, char* argv[]) {
	// initialize MPI
	if (MPI_Init(argc, &argv) != MPI_SUCCESS) {
		std::cerr << "MPI_Init failed." << std::endl;
	}

	// determine local MPI rank
	MPI_Comm_rank(MPI_COMM_WORLD, &mpiRank);
	// determine total number of processes
	MPI_Comm_size(MPI_COMM_WORLD, &numberOfProcesses);

	//determine if there is any Graph
	graphExists=false;
	rankOfGraph=-1;
	int ownStatus[] = { SWE };
	int recv[numberOfProcesses];

	MPI_Allgather(&ownStatus, 1, MPI_INT, &recv, 1, MPI_INT, MPI_COMM_WORLD);


	for (int i = 0; i < numberOfProcesses; i++) {
		if(recv[i]!=SWE){
			graphExists=true;
			rankOfGraph=recv[i];
			printf("graph exists: %d\n", rankOfGraph);
		}
	}

	//creats a Communicator without the GRAPH for the maxTimeStep reduction
//	int excl[]={2};
//	MPI_Group orig_group, groupWithoutGraph;
//	MPI_Comm_group(MPI_COMM_WORLD, &orig_group);
//	MPI_Group_excl(orig_group, graphExists?1:0, excl, &groupWithoutGraph);
//	MPI_Comm_create(MPI_COMM_WORLD, groupWithoutGraph, &commWithoutGraph);



	//MPIDatatype mpiPoint
	const int nitems=5;
	int blocklengths[5]={1,1,1,1,1};
	MPI_Datatype types[5] = {MPI_INT, MPI_INT, MPI_FLOAT, MPI_FLOAT, MPI_FLOAT};
	MPI_Aint offsets[5];
	offsets[0] = (int) offsetof(struct PointForMPI, x);
	offsets[1] = (int) offsetof(struct PointForMPI, y);
	offsets[2] = (int) offsetof(struct PointForMPI, h);
	offsets[3] = (int) offsetof(struct PointForMPI, b);
	offsets[4] = (int) offsetof(struct PointForMPI, time);

	MPI_Type_create_struct(nitems, blocklengths, offsets, types, &mpiPoint);
	MPI_Type_commit(&mpiPoint);

	//printf("piPoint\n");


	// determine the layout of MPI-ranks: use blocksX*blocksY grid blocks
	blocksY = computeNumberOfBlockRows();
	blocksX = getNumberOfProcesses() / blocksY;

	// determine local block coordinates of each SWE_Block
	blockPositionX = getMpiRank() / blocksY;
	blockPositionY = getMpiRank() % blocksY;


	// compute MPI ranks of the neighbour processes

	leftNeighborRank =
		(blockPositionX > 0) ? getNeighbor(mpiRank - blocksY) : MPI_PROC_NULL;
	rightNeighborRank =
		(blockPositionX < blocksX - 1) ? getNeighbor(mpiRank + blocksY) : MPI_PROC_NULL;
	bottomNeighborRank =
		(blockPositionY > 0) ? getNeighbor(mpiRank - 1) : MPI_PROC_NULL;
	topNeighborRank =
		(blockPositionY < blocksY - 1) ? getNeighbor(mpiRank + 1) : MPI_PROC_NULL;
	//printf("MPI_FRAVE: init done");
}

/**
 * closes the MPI Connection
 */
void MPI_FRAVE::finalize() {

	MPI_Finalize();
}

/**
 * terminates the Program
 */
void MPI_FRAVE::abort() {
	MPI_Abort(MPI_COMM_WORLD, -1);
}


/**
 * guarantees that all Processes use the same width and height for computation
 */
void MPI_FRAVE::setSize(int &w, int &h) {


	int globalW=0;
	MPI_Allreduce(&w, &globalW, 1, MPI_INT,
			MPI_MAX, MPI_COMM_WORLD);

	int globalH=0;
	MPI_Allreduce(&h, &globalH, 1, MPI_INT,
			MPI_MAX, MPI_COMM_WORLD);

	height=globalH;
	width=globalW;
	h=globalH;
	w=globalW;
}
/**
 * Exchanges the left and right ghost layers with MPI's SendReceive.
 *
 * @param o_leftInflow ghost layer, where the left neighbor writes into.
 * @param i_leftOutflow layer where the left neighbor reads from.
 * @param o_rightInflow ghost layer, where the right neighbor writes into.
 * @param i_rightOutflow layer, where the right neighbor reads form.
 */
void MPI_FRAVE::exchangeLeftRightGhostLayers(SWE_Block1D* o_leftInflow,
		SWE_Block1D* i_leftOutflow, SWE_Block1D* o_rightInflow,
		SWE_Block1D* i_rightOutflow) {

	MPI_Status l_status;

	// send to left, receive from the right:
	//printf("Tag: %d, count: %d, error: %d\n",l_status.MPI_TAG,
	//		l_status.count_lo, l_status.MPI_ERROR);


	if(!ColSet){
		//! MPI col-vector: 1 block, width+2 elements per block, stride
		MPI_Type_vector(height + 2, 1, o_leftInflow->h.getStride(), MPI_FLOAT, &mpiCol);
		MPI_Type_commit(&mpiCol);
		printf("ColStride: %d", o_leftInflow->h.getStride());
		ColSet=true;
	}

	int elem = 1;
	MPI_Sendrecv(i_leftOutflow->h.elemVector(), elem, mpiCol,
			leftNeighborRank, 1, o_rightInflow->h.elemVector(), elem, mpiCol,
			rightNeighborRank, 1, MPI_COMM_WORLD, &l_status);


	MPI_Sendrecv(i_leftOutflow->hu.elemVector(), elem, mpiCol,
			leftNeighborRank, 2, o_rightInflow->hu.elemVector(), elem,
			mpiCol, rightNeighborRank, 2, MPI_COMM_WORLD, &l_status);

	MPI_Sendrecv(i_leftOutflow->hv.elemVector(), elem, mpiCol,
			leftNeighborRank, 3, o_rightInflow->hv.elemVector(), elem,
			mpiCol, rightNeighborRank, 3, MPI_COMM_WORLD, &l_status);

	// send to right, receive from the left:
	MPI_Sendrecv(i_rightOutflow->h.elemVector(), elem, mpiCol,
			rightNeighborRank, 4, o_leftInflow->h.elemVector(), elem, mpiCol,
			leftNeighborRank, 4, MPI_COMM_WORLD, &l_status);

	MPI_Sendrecv(i_rightOutflow->hu.elemVector(), elem, mpiCol,
			rightNeighborRank, 5, o_leftInflow->hu.elemVector(), elem,
			mpiCol, leftNeighborRank, 5, MPI_COMM_WORLD, &l_status);

	MPI_Sendrecv(i_rightOutflow->hv.elemVector(), elem, mpiCol,
			rightNeighborRank, 6, o_leftInflow->hv.elemVector(), elem,
			mpiCol, leftNeighborRank, 6, MPI_COMM_WORLD, &l_status);
}

/**
 * Exchanges the bottom and top ghost layers with MPI's SendReceive.
 *
 * @param o_bottomNeighborInflow ghost layer, where the bottom neighbor writes into.
 * @param i_bottomNeighborOutflow host layer, where the bottom neighbor reads from.
 * @param o_topNeighborInflow ghost layer, where the top neighbor writes into.
 * @param i_topNeighborOutflow ghost layer, where the top neighbor reads from.
 */
void MPI_FRAVE::exchangeBottomTopGhostLayers(
		SWE_Block1D* o_bottomNeighborInflow, SWE_Block1D* i_bottomNeighborOutflow,
		SWE_Block1D* o_topNeighborInflow, SWE_Block1D* i_topNeighborOutflow) {


	if(!RowSet){
		//! MPI row-vector: 1 block, height+2 elements per block, stride of 1
		MPI_Type_vector(width + 2, 1, o_bottomNeighborInflow->h.getStride(), MPI_FLOAT, &mpiRow);
		MPI_Type_commit(&mpiRow);
		printf("RowStride: %d", o_bottomNeighborInflow->h.getStride());
		RowSet=true;
	}


	MPI_Status l_status;

	int elem = 1;
	// send to bottom, receive from the top:
	MPI_Sendrecv(i_bottomNeighborOutflow->h.elemVector(), elem, mpiRow,
			bottomNeighborRank, 11, o_topNeighborInflow->h.elemVector(), elem,
			mpiRow, topNeighborRank, 11, MPI_COMM_WORLD, &l_status);

	MPI_Sendrecv(i_bottomNeighborOutflow->hu.elemVector(), elem, mpiRow,
			bottomNeighborRank, 12, o_topNeighborInflow->hu.elemVector(), elem,
			mpiRow, topNeighborRank, 12, MPI_COMM_WORLD, &l_status);

	MPI_Sendrecv(i_bottomNeighborOutflow->hv.elemVector(), elem, mpiRow,
			bottomNeighborRank, 13, o_topNeighborInflow->hv.elemVector(), elem,
			mpiRow, topNeighborRank, 13, MPI_COMM_WORLD, &l_status);

	// send to top, receive from the bottom:
	MPI_Sendrecv(i_topNeighborOutflow->h.elemVector(), elem, mpiRow,
			topNeighborRank, 14, o_bottomNeighborInflow->h.elemVector(), elem,
			mpiRow, bottomNeighborRank, 14, MPI_COMM_WORLD, &l_status);

	MPI_Sendrecv(i_topNeighborOutflow->hu.elemVector(), elem, mpiRow,
			topNeighborRank, 15, o_bottomNeighborInflow->hu.elemVector(), elem,
			mpiRow, bottomNeighborRank, 15, MPI_COMM_WORLD, &l_status);

	MPI_Sendrecv(i_topNeighborOutflow->hv.elemVector(), elem, mpiRow,
			topNeighborRank, 16, o_bottomNeighborInflow->hv.elemVector(), elem,
			mpiRow, bottomNeighborRank, 16, MPI_COMM_WORLD, &l_status);

}

/**
 * exchanges the maximum timestep between all Processes
 * @param l_maxTimeStepWidth max timestep of this Process
 * @return maximum timestep that can be used for the whole simulated area
 */
float MPI_FRAVE::maxTimeStep(float l_maxTimeStepWidth) {
	// determine smallest time step of all blocks
	float l_maxTimeStepWidthGlobal = 0;
	MPI_Allreduce(&l_maxTimeStepWidth, &l_maxTimeStepWidthGlobal, 1, MPI_FLOAT,
			MPI_MIN, MPI_COMM_WORLD);
	return l_maxTimeStepWidthGlobal;
}


/**
 * Exchanges the status messages to handle user interaction
 * @param doReset gets set true for restart od the current scenario or a new scenario
 * @param stop currently unused
 * @param newScenario gets set true if a new scenario should be started
 * @param scenario number of the next scenario
 */
void MPI_FRAVE::exchangeStatus(bool &doReset, bool &stop, bool &newScenario,
		int &scenario) {	/*
	 * 100 running
	 * 101 reset
	 * 102 stop
	 *
	 * 0-NUM_OF_SCENARIOS sceanrios
	 */
	int ownStatus[] = { RUNNING };
	if (doReset)
		ownStatus[0] = RESET;
	if (stop)
		ownStatus[0] = STOP;
	if (newScenario)
		ownStatus[0] = scenario;

	int recv[numberOfProcesses];
	MPI_Allgather(&ownStatus, 1, MPI_INT, &recv, 1, MPI_INT, MPI_COMM_WORLD);

	for (int i = 0; i < numberOfProcesses; i++) {
		switch (recv[i]) {
		case RESET:
			doReset = true;
			break;
		case STOP:
			stop = true;
			break;
		case REGISTER_POINT:
			registerPoint();
			break;
		default:
			if (recv[i] >= 0 && recv[i] <= num_of_scenarios) {
				scenario = recv[i];
				doReset = true;
				newScenario = false;
				
				for(int i=0;i<numPoints;i++){
					points[i].b = NAN;
				}
			}
		}

	}
}

/**
 * @return number of Block Rows
 */
int MPI_FRAVE::computeNumberOfBlockRows() {
	int numberOfCols = std::sqrt(getNumberOfProcesses());
	while (getNumberOfProcesses() % numberOfCols != 0)
		numberOfCols--;
	return numberOfCols;
}

/**
 * returns the current MPI-Rank as if there were no Graph-classes
 */
int MPI_FRAVE::getMpiRank() {
	if(!graphExists)
		return mpiRank;
	else{
		if(mpiRank<rankOfGraph)
			return mpiRank;
		else return mpiRank-1;
	}
}

/**
 * returns the number of non Graph processes
 */
int MPI_FRAVE::getNumberOfProcesses() {
	if(graphExists)return numberOfProcesses-1;
	else return numberOfProcesses;
}

/**
 * sends the Data of all registered Points
 * @param d_h Pointer to the device address where the height data is
 * @param d_b Pointer to the device address where the bathymetry data is
 * @param time current time of simulation
 */
void MPI_FRAVE::transmitPoints(const float *d_h, const float *d_b, float time){
	if(graphExists && rankOfGraph>=0){
		for(int i=0;i<numPoints;i++){
			if (points[i].b != points[i].b){
				int pos1D = computeOneDPositionKernel(points[i].x + 1, points[i].y + 1, height + 2);
				float b[1];
				cudaMemcpy(b, &d_b[pos1D], sizeof(float), cudaMemcpyDeviceToHost);
				checkCUDAError("copying Point-Bath to Host");
				points[i].b = b[0];
			}

			transmitPoint(points[i].x, points[i].y, d_h, points[i].b, time);
		}
	}
}

/**
 * Transmits a single Point
 * @param x Koordinate of the Point
 * @param y Koordinate of the Point
 * @param d_h d_h Pointer to the device address where the height data is
 * @param b bathymetry value for this Point
 * @param time current time of simulation
 */
void MPI_FRAVE::transmitPoint(int x, int y, const float *d_h, float b, float time){

	//ghost/copy layer may not be used
	int pos1D = computeOneDPositionKernel(x+1, y+1, height+2);
	float h[1];
	//printf("x: %d y: %d\n", x, y);

	//get Data
	cudaMemcpy(h, &d_h[pos1D], sizeof(float), cudaMemcpyDeviceToHost);
			checkCUDAError("copying Point to Host");


	//pack with global coords
	PointForMPI s={x+blockPositionX * width, y + blockPositionY * height, h[0], b, time};

	//send
	MPI_Request* req=new MPI_Request();
	//printf("send: x: %d, y: %d, h: %f, b: %f, t: %f", s.x, s.y, s.h, s.b, s.time);
	MPI_Isend(&s, 1, mpiPoint, rankOfGraph, POINT_TAG_OLD, MPI_COMM_WORLD, req);
}


/**
 * a new Point is registered
 */
void MPI_FRAVE::registerPoint(){

	int rec[2];
	MPI_Status status;
	MPI_Bcast(rec, 2, MPI_INT, rankOfGraph, MPI_COMM_WORLD);

	Point t;
	t.x=rec[0];
	t.y=rec[1];

	//printf("INPUT: x: %d, y: %d\n", t.x, t.y);
	Point p;
	//printf("x: %d, y: %d", t.x, t.y);
	if (t.x >= blockPositionX * width && t.x < (blockPositionX + 1) * width &&
		t.y >= blockPositionY * height && t.y < (blockPositionY + 1) * height) {
		//printf("x: %d, y: %d, minX: %d, maxX: %d", t.x, t.y, blockPositionX * width, (blockPositionX + 1) * width);
		p.x = t.x - blockPositionX * width;
		p.y = t.y - blockPositionY * height;


		if(points==NULL){
			points=(Point *)malloc(sizeof(Point));
		}else{
			Point *u=(Point *)malloc((numPoints+1)*sizeof(Point));
			//copy old values
			for(int i=0;i<numPoints;i++){
				u[i]=points[i];
			}
			free(points);
			points=u;
		}
		//add new Point
		points[numPoints].x=p.x;
		points[numPoints].y=p.y;
		points[numPoints].b = NAN;
		numPoints++;

	}

}

/**
* @param i rank of MPI-neighbor without an Graph Process
* returns the MPI-Rank of neighbor with Graph Process
*/
int MPI_FRAVE::getNeighbor(int i){
	if(!graphExists)return i;
	if(rankOfGraph<0)return i; //should never happen
	if ((rankOfGraph < i && i < mpiRank) || (rankOfGraph > i && i > mpiRank))return i;
	if(i < mpiRank)
		return rankOfGraph < mpiRank ? i-1 : i;
	else
		return rankOfGraph > mpiRank ? i+1 : i;
}


