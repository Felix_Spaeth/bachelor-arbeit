
---------Visualisierungsprogramm-----------------

Zum Ausf�hren des Visualisierungsprogramms unter SWE_FRAVE/ muss dieses mittels

scons [netCDFDir=/Pfad/zu/netCDF/] 

kompiliert werden.
Hierf�r muss scons, g++, freeGlut, GLEW, MPI, netCDF und das CUDA Toolkit installiert sein.

Zum Ausf�hren auf einem Knoten reicht es die erstellte Datei auszuf�hren.

Zum Ausf�hren in mehreren Instanzen kann das unter SWE_FRAVE enthaltene run_Test skript verwendet werden.

Die netCDF Dateien m�ssen sich in dem verzeichnis Befinden, aus das Programm gestartet wird.
Die Dateien k�nnen unter folgenden Link heruntergeladen werden:
http://www5.in.tum.de/lehre/praktika/swe_lab_ss13/scenarios/

---------Graph-Programm-------------------------


Zum kompilieren des Graphprogramms, werden zus�tzlich fltk und mathGL ben�tigt.

anschlie�end das makefile so anpassen, dass die Includes stimmen und mit make kompilieren.

Das erstellte GRAPH Programm ist ohne visualisierung lauff�hig, zum Anzeigen von Messpunkten wird jedoch das Visualisierungsprogramm ben�tigt.



--------------------------------------------------
Das runMPITest Skript startet sowohl die Visualisierung als auch das Graphprogramm

